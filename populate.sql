# database encoding
ALTER DATABASE twitter_db CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci;

# table creation
drop table if exists tweets;
SET NAMES utf8mb4;
create table tweets (
    uid varchar(20) not null,
    tid varchar(20) not null,
    creation timestamp not null,
    content VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
    # location varchar(100),
    PRIMARY KEY (uid,tid)
);

ALTER TABLE tweets CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE tweets CHANGE content content varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
# REPAIR TABLE tweets;
# OPTIMIZE TABLE tweets;

drop table if exists stats;

create table stats (
    uid varchar(20) not null,
    tid varchar(20) not null,
    creation timestamp not null,
    # content VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
    # location varchar(100),
    EMO int default 0,
    PUN int default 0,
    URL int default 0,
    MS int default 0,
    OTHER int default 0,
    QT int default 0,
    PAR int default 0,
    NN int default 0,
    NNP int default 0,
    NNPS int default 0,
    NNS int default 0,
    VB int default 0,
    VBD int default 0,
    VBG int default 0,
    VBN int default 0,
    VBP int default 0,
    VBZ int default 0,
    CC int default 0,
    CD int default 0,
    DT int default 0,
    EX int default 0,
    FW int default 0,
    IN_ int default 0,
    JJ int default 0,
    JJR int default 0,
    JJS int default 0,
    LS int default 0,
    MD int default 0,
    PDT int default 0,
    POS int default 0,
    PRP int default 0,
    PRP_S int default 0,
    RB int default 0,
    RBR int default 0,
    RBS int default 0,
    RP int default 0,
    SYM int default 0,
    TO_ int default 0,
    UH int default 0,
    WDT int default 0,
    WP int default 0,
    WP_S int default 0,
    WRB int default 0,
    PRIMARY KEY (uid,tid)
);
# alter table stats add column content  VARCHAR(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
# alter table stats drop column content;
ALTER TABLE stats CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
# ALTER TABLE stats CHANGE content content varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;
# REPAIR TABLE stats;


# Load data from CSV
LOAD DATA LOCAL INFILE '~/Desenvolvimento/twitter/data.csv' INTO TABLE tweets
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n'
(uid, tid, creation, content);

# Delete users with less than 60 tweets
SET SQL_SAFE_UPDATES = 0;
DELETE FROM tweets 
WHERE
    uid IN (SELECT 
        uid
    FROM
        (SELECT 
            uid, COUNT(tid) ctt
        FROM
            tweets
        GROUP BY uid
        HAVING ctt < 60) AS t);
SET SQL_SAFE_UPDATES = 1;

# Clean null user
set sql_safe_updates = 0;
delete from tweets where uid = '';
set sql_safe_updates = 1;

select * from stats;
select * from tweets; 