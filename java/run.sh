#!/bin/bash
export CLASSPATH=$CLASSPATH:~/Programas/weka-3-8-2/weka.jar:.

    for attr in {1..42}
    do
        java weka.filters.unsupervised.attribute.Remove -R $attr -i in.arff -o out.arff
        echo "-- RUNNING $classifier WITHOUT ATTRIBUTE $attr ----------------"
        java "weka.classifiers.$1" -t out.arff
    done

    echo "-- RUNNING $1 WITH ALL ATTRIBUTES ------------------------"
    java "weka.classifiers.$1" -t in.arff

