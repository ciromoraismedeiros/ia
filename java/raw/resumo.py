# -*- coding:utf-8 -*-

import sys

f = open(sys.argv[1])

copy_count = 0
must_print = False

for l in f.readlines():
    if l.startswith('-- RUNNING'):
        start = len('-- RUNNING WITHOUT ATTRIBUTE ')+1
        attr = l[start:start+2]

    if l.startswith('=== Stratified cross-val'):
        must_print = True

    if must_print and l.startswith('Correctly Classified'):
        hits = l[-10:-6]

    if must_print and l.startswith('Incorrectly Classified'):
        miss = l[-10:-6]

        print attr,',',hits,',',miss
        must_print = False

f.close()
