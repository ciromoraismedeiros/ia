import MySQLdb
import nltk # nltk.help.upenn_tagset()
from nltk.tokenize.casual import TweetTokenizer
from nltk.tag.perceptron import PerceptronTagger
import re

url_exp = re.compile('(http(s)?://)?[a-zA-Z]([a-zA-Z]|[0-9])*\..+')

def tag2sql(word, tag):
    if repr(word).startswith("u'\U"):
        return 'EMO'
    elif url_exp.match(word) != None:
        return 'URL'
    elif word == '%' or tag == '$':
        return 'MS'
    elif tag in ["''",'``']:
        return 'QT'
    elif tag in ['(',')']:
        return 'PAR'
    elif tag in [',','.',';',':','--']:
        return 'PUN'
    elif tag == 'PRP$':
        return 'PRP_S'
    elif tag == 'WP$':
        return 'WP_S'
    elif tag == 'IN':
        return 'IN_'
    elif tag == 'TO':
        return 'TO_'
    elif tag in ['NN', 'NNP', 'NNPS', 'NNS', 'VB', 'VBD', 'VBG', 'VBN', 
        'VBP', 'VBZ', 'CC', 'CD', 'DT', 'EX', 'FW', 'JJ', 'JJR', 
        'JJS', 'LS', 'MD', 'PDT', 'POS', 'PRP', 'RB', 'RBR', 'RBS',
        'RP', 'SYM', 'UH', 'WDT', 'WP', 'WP_S', 'WRB']:
        return tag
    else:
        return 'OTHER'
 
db = MySQLdb.connect(host="localhost",  # your host 
                     user="ciro",       # username
                     passwd="123",      # password
                     db="twitter_db",   # name of the database
                     charset='utf8mb4')
 
# Create a Cursor object to execute queries.
cur = db.cursor()

# Select data from table using SQL query.
cur.execute("SELECT * FROM tweets")

for row in cur.fetchall():
    tags = nltk.pos_tag(TweetTokenizer().tokenize(row[3]))
    #~ tags = nltk.pos_tag(nltk.word_tokenize(row[3]))
    #~ print(tags)
    stats = {}
    for s,t in tags:
        key = tag2sql(s,t)
        stats[key] = stats.get(key,0)+1
    #~ print(stats)

    if len(stats)>0:
        columns = 'uid, tid, creation'
        values  = str(row[0])+', '+str(row[1])+", '"+str(row[2])+"'"
        
        while len(stats)>0:
            (col, val) = stats.popitem()
            columns += ', '+col
            values  += ', '+str(val)
        command = 'insert IGNORE into stats ('+columns+') values ('+values+')'
        print(command)
        
        cur.execute(command)
cur.execute('commit')
cur.close()

