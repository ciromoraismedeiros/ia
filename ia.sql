
# Users stats
# SELECT 
#     user_id, MAX(len), MIN(len), AVG(len)
# FROM
#     (SELECT 
#         user_id, tweet_id, LENGTH(text) AS len
#     FROM
#         tweet_db.tweets) t group by user_id

# Load data from CSV
LOAD DATA LOCAL INFILE '~/Downloads/# all_tweets.csv' INTO TABLE tweets
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"' 
LINES TERMINATED BY '\r\n'
(uid, tid, creation, content);

select count(*) from tweets;


# Delete users with less than 60 tweets
# SET SQL_SAFE_UPDATES = 0;
# DELETE FROM tweets 
# WHERE
#     user_id IN (SELECT 
#         user_id
#     FROM
#         (SELECT 
#             user_id, COUNT(tweet_id) ctt
#         FROM
#             tweets
#         GROUP BY user_id
#         HAVING ctt < 60) AS t);
# SET SQL_SAFE_UPDATES = 1;

# Clean null user
# delete from tweet_db.tweets where user_id = ""


# Rename column
# ALTER TABLE tweet_db.tweets CHANGE column `text` `content` varchar(400)


# Modify encoding
# alter table tweet_db.tweets MODIFY content VARCHAR(400) CHARACTER SET utf8

-- create database twitter_db;

-- create table twitter_db.tweets (
-- 	uid varchar(20),
--     tid varchar(20),
--     creation timestamp,
--     content varchar(300),
--     -- location varchar(100)
-- )