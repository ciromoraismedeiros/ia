# -*- coding:utf-8 -*-
from sql import db, tag2sql, taglist2sql
import nltk # nltk.help.upenn_tagset()
from nltk.tokenize.casual import TweetTokenizer
import ngramsets

class TreeNode:
    def __init__(self, label):
        self.label  = label
        self.weight = 0
        self.children = {}

    def add(self, label):
        if label not in self.children:
            new = TreeNode(label)
            self.children[label] = new
        self.children[label].weight += 1
        return self.children[label]

    def show(self,trace=''):
        if len(self.children) > 0:
            for c in self.children.values():
                c.show(trace+self.label+'('+str(self.weight)+').')
        else:
            print trace+self.label+'('+str(self.weight)+')'

    def show2(self,offset=0):
        print self.label+'('+str(self.weight)+')',
        for c in self.children.values():
            c.show2(offset+len(self.label+'('+str(self.weight)+')'))
            print ' '*(offset+len(self.label+'('+str(self.weight)+')')),
        print ''
        

def build_user_profile(user, sentences):
    g = ngramsets.NGramSet(user)

    for tags in sentences:
        last = 'start'
        #~ print tags
        #~ raw_input("enter")
        for tag in tags:
            g.add(last, tag, 1.0)#/(len(tags)+2.0))
            last = tag
        g.add(last, 'end', 1.0)#/(len(tags)+2.0))
    
    return g

def query_user_sentences(user, num_tweets):
    command = "SELECT * FROM tweets where uid="+user+" LIMIT "str(num_tweets)
    cursor = db.cursor()
    cursor.execute(command)

    sentences = []
    for row in cursor.fetchall():
        #terms = taglist2sql(nltk.pos_tag(TweetTokenizer().tokenize(row[3])))
        terms = TweetTokenizer().tokenize(row[3])
        #print row[3], tags
        sentences += [terms]
            
    cursor.close()
    return sentences

def sort(v):
    for i in range(len(v)):
        for j in range(i+1,len(v)):
            _, vi = v[i]
            _, vj = v[j]
            if vi < vj:
                aux  = v[i]
                v[i] = v[j]
                v[j] = aux
    return v

if __name__ == '__main__':

    users = [
        '1043583414',
        '104707402',
        #~ '10470852',
        '1010027624',
        '101808366',
        #~ '101989649',
        #~ '102258551',
        '1024978375',
        '102606065',
        #~ '103484857',
        #~ '104975642',
        #~ '105110566',
        #~ '1059047846',
        #~ '1061343566',
        '1064595061',
        '106908311',
        '106929956',
        '1072691827',
        '107569907',
        '108313409',
        '1084038019',
        '1086924032',
        #~ '1090729028',
        #~ '109079860',
        '109597858',
        '1104602792',
        '1108268060',
        '111209593',
        '1118033364',
        '112244228',
        '1125738913',
        #~ '114053642',
        #~ '1141549836',
        #~ '1141735194',
        #~ '114587132',
        #~ '1152052632',
        '1152734053',
        '1158847736',
        '1161517392',
        '1170659856',
        '1173469999',
        '1175529914',
        '117841811',
        '118198929',
        '118821838',
        '1196110825',
    ]
    
    #~ users = [
        #~ '1043583414']
    profiles = []
    for u in users:
        p = build_user_profile(u, query_user_sentences(u))
        profiles.append(p)
        #~ for s,w,o in p:
            #~ print s,w,o

    #tweet = 'I should be on my way to coachella but my ass is going to school instead cause getting education is cooler than having the best time of your life'
    tweet = "I miss mar"
    tweet = ".@McAlistersDeli Adam Sandler shared his group's strategy on building &amp; growing their business with @franupdatemedia. Great to see our franchisees being looked to as leaders in the industry! https://t.co/f0ObDkhLbw"
    tweet = ".SaxtonGroupTX invites the community of each new restaurant to experience a taste of @McAlistersDeli the day before opening. When they opened their 76th location in Wylie TX, they served over $7K in food in 3 hrs! Great way to introduce our hospitality &amp; delicious food! #McACrew https://t.co/KcG"
    #new = taglist2sql(nltk.pos_tag(TweetTokenizer().tokenize(tweet)))
    new = TweetTokenizer().tokenize(tweet)
    print new
        # [(u"It's", 'NNP'), (u'only', 'RB'), (u'night', 'NN'), (u'.', 'PUN')]
    acc = []
    for p in profiles:
        acc += [(p.user, p.similarity(new))]
    
    for u,s in sort(acc):
        print "{0:.2f}".format(round(s,2)),'=',u
        

