#!/usr/bin/env python
# encoding: utf-8

import tweepy #https://github.com/tweepy/tweepy
import csv

#Twitter API credentials
consumer_key = "g5xkD3ppBHiSCfjHIDSNQlJlg"
consumer_secret = "sNqBniXqRjffL2AIygQpCwaRsflMc2zaVq60ZZl8r8mgVYhg6f"
access_key = "322859032-TnnuHjGZsdAEVHTaIsfEM40EjzgWTxMAbuU4C37U"
access_secret = "QxLzmx8CBuZfeaVnPmAaKjbRRIHrwHdxGp1ZDhIwj9dML"

COUNT = 60

def get_all_tweets(user_id):
    #Twitter only allows access to a users most recent 3240 tweets with this method
    
    #authorize twitter, initialize tweepy
    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_key, access_secret)
    api = tweepy.API(auth)
    
    #initialize a list to hold all the tweepy Tweets
    alltweets = []    
    
    #make initial request for most recent tweets (200 is the maximum allowed count)
    new_tweets = api.user_timeline(user_id = user_id, count=COUNT, include_rts=False, exclude_replies=True, languages=['en'], tweet_mode='extended')
    
    #save most recent tweets
    for t in new_tweets:
        if t.lang=='en':
            alltweets.append(t)
    
    #save the id of the oldest tweet less one
    if len(alltweets)>0:
        oldest = alltweets[-1].id - 1
        old_len = 0
        
        #keep grabbing tweets until there are no tweets left to grab
        while len(new_tweets) > 0 and len(alltweets) < COUNT:
            print "getting tweets before %s" % (oldest)
            
            #all subsiquent requests use the max_id param to prevent duplicates
            new_tweets = api.user_timeline(user_id = user_id, count=COUNT, include_rts=False, exclude_replies=True, languages=['en'], max_id=oldest, tweet_mode='extended')
            
            if len(new_tweets) == old_len:
                break
            old_len = len(new_tweets)
            
            #save most recent tweets
            for t in new_tweets:
                if t.lang=='en':
                    alltweets.append(t)
            
            #update the id of the oldest tweet less one
            oldest = alltweets[-1].id - 1
            
            print "...%s tweets downloaded so far" % (len(alltweets))
        else:
            #transform the tweepy tweets into a 2D array that will populate the csv    
            outtweets = [[user_id, tweet.id_str, tweet.created_at, tweet.full_text.encode("utf-8")] for tweet in alltweets]
    
            #write the csv    
            with open('all_tweets.csv', 'ab') as f:
                writer = csv.writer(f)
                # ~ writer.writerow(['user',"id","created_at","text"])
                writer.writerows(outtweets)
                f.close()


def load_users():
    users = []
    f = open('users.txt','r')
    for rawid in f.readlines():
        users.append(int(rawid))
    f.close()
    return users


if __name__ == '__main__':
    #pass in the username of the account you want to download
    for u in load_users():
		try:
			get_all_tweets(u)
		except tweepy.TweepError:
			continue


