# -*- coding:utf-8 -*-
def build_ngrams(words, n=2, case=''):
    terms = []
    case_words = []
    for w in words:
        if case == 'lower':
            w = w.lower()
        elif case == 'upper':
            w = w.upper()
        case_words += [w]
        
    for i in range(len(case_words)-n):
        terms.append(tuple(case_words[i:i+n]))
    return terms


class ProfileSet:
    def __init__(self):
        self.profiles = {}
        self.terms = {}
        
    def add_term(self, author, term, weight=1):
        if term not in self.terms:
            self.terms[term] = 0
        self.terms[term] += 1
        if author not in self.profiles:
            self.profiles[author] = Profile(author)
        self.profiles[author].add(term)

    def weight(self, term):
        return 1.0/self.terms.get(term,1)

    def spi(self, author, terms):
        score = 0.0
        for term in terms:
            if term in self.profiles[author].terms:
                score += 1
        return score

    def wpi(self, author, terms):
        score = 0.0
        count = 0
        for term in terms:
            if term in self.profiles[author].terms:
                score += self.weight(term)
                count += 1
        return score/count if count > 0 else score

    def wpi_nomean(self, author, terms):
        score = 0.0
        for term in terms:
            if term in self.profiles[author].terms:
                score += self.weight(term)
        return score
    
    def w2pi(self, author, terms):
        score = 0.0
        for term in terms:
            score += self.weight(term)*self.profiles[author].weight(term)
        return score

    def author_of(self,terms,func):
        author = None
        score  = -1
        for p in self.profiles:
            s = func(p, terms)
            if s > score:
                author = p
                score  = s
        return (author,score)

    def restrict_terms(self,L):
        for key in self.profiles:
            top = []
            for t in self.profiles[key].terms:
                top += [(t,self.weight(t))]
            for i in range(len(top)-1):
                for j in range(i,len(top)):
                    ti,vi = top[i]
                    tj,vj = top[j]
                    
                    if vi < vj:
                        aux = top[i]
                        top[i] = top[j]
                        top[j] = aux
            #~ print top[:L]
            #~ print ''
            
            new_terms = {}
            for t,v in top[:L]:
                new_terms[t] = v
            self.profiles[key].terms = new_terms
        
class Profile:
    def __init__(self,author):
        self.author = author
        self.terms = {}
    
    def add(self, term, weight=1):
        if term not in self.terms:
            self.terms[term] = 0.0
        self.terms[term] += weight

    def weight(self, term):
        return self.terms.get(term,0.0)

    def show(self):
        print self.author
        for term in self.terms:
            print term,self.terms[term]

    
