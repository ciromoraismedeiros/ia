class NGramSet:
    def __init__(self,user):
        self.user = user
        self.data = {}
        self.count = 0

    def __len__(self):
        return self.count

    def __iter__(self):
        for (user,ngram) in self.data:
            yield (user,ngram,self.weight(user,ngram))
    
    def add(self, user, ngram, weight=1):
        if (user,ngram) not in self.data:
            self.data[(user,ngram)] = 0.0
            self.count += 1
        self.data[(user,ngram)] += weight

    def weight(self, user, ngram):
        return self.data.get((user,ngram),0.0)

    def similarity(self, tags):
        score = 0.0
        last = 'start'
        for tag in tags:
            #score += self.weight(last,tag)
            score += float(self.weight(last,tag))#/(len(tags)+2)
            last = tag
        score += (self.weight(last,'end'))#/(len(tags)+2))

        return score
        

