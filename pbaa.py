# -*- coding:utf-8 -*-
from sql import db, tag2sql, taglist2sql
import nltk # nltk.help.upenn_tagset()
from nltk.tokenize.casual import TweetTokenizer
from profiles import *
import random
import re
from user_list import all_users

def query_user_tweets(user, num_tweets=50):
    command = "SELECT content FROM tweets where uid="+user+" order by tid LIMIT "+str(num_tweets)
    cursor = db.cursor()
    cursor.execute(command)

    sentences = []
    for row in cursor.fetchall():
        sentences += [row[0]]

    cursor.close()
    return sentences

def tokenize(tweets, char_or_word='word'):
    tokenized = []
    if char_or_word == 'word':
        for t in tweets:
            tokenized += [TweetTokenizer().tokenize(t)]
    elif char_or_word == 'char':
        for t in tweets:
            tokenized += [list(t)]
    return tokenized

def clean(raw_tweets):
    domain_re = re.compile('(http(s)?://)?[a-zA-Z]([a-zA-Z0-9])*(\.[a-zA-Z]([a-zA-Z0-9]))+')
    number_re = re.compile('\d+')
    #time_re = re.compile('\d(:|h|H|hr)\d(:|m|M|min)\d(s|S|sec|am|a.m.|a.m|pm|p.m.|p.m)')
    
    tweets = []
    
    for raw_tweet in raw_tweets:
        tweet = []
        for raw_word in raw_tweet:
            word = raw_word#.lower()

            # extracting domain from URL's
            domain = re.match(domain_re, word)
            # checking whether the word is a number
            number = re.match(number_re, word)
            new = []
            if domain != None:
                word = word[domain.start():domain.end()]
                #~ print word
                new = [word]
            #~ elif word.startswith('#'):
                #~ word = '###'
            elif number != None:
                word = '999'
                new = [word]
            else:
                new = [word]
            tweet += new
        #~ print tweet
        tweets += [tweet]
    return tweets


def main(n, training_size, testing_size, number_of_users, L=-1, char_or_word='word', case='', repetitions=10):
    #~ Selecting users
    users = set()
    for i in random.sample(xrange(len(all_users)),number_of_users):
        users.add(all_users[i])
        
    #~ Accuracy measures
    wpi = {}
    spi = {}
    w2pi = {}
    wpi_nomean = {}

    #~ Collecting tweets
    user_tweets = {}
    for u in users:
        user_raw_tweets = query_user_tweets(u, training_size+testing_size)
        tk_tweets = tokenize(user_raw_tweets)
        user_tweets[u] = clean(tk_tweets)
    
    for j in range(repetitions):
        testing_indexes = set(random.sample(xrange(training_size),testing_size))
        testing_set     = []
    
        #~ Training
        P = ProfileSet() # profiles set
        for u in user_tweets:
            for i, tweet in enumerate(user_tweets[u]):
                terms = build_ngrams(tweet, n, case)
                if i in testing_indexes:
                    # save for testing
                    testing_set.append((u,terms))
                else:
                    # use for training
                    for term in terms:
                        P.add_term(u,term)
        if L > 0:
            P.restrict_terms(L) # keeps only L top terms
        
        #~ Testing 
        for author,terms in testing_set:
            ats,scores = P.author_of(terms, P.spi)
            if author not in spi:
                spi[author] = 0
            if author == ats:
                spi[author] += 1
            
            atw,scorew = P.author_of(terms, P.wpi)
            if author not in wpi:
                wpi[author] = 0
            if author == atw:
                wpi[author] += 1

            atw2,scorew2 = P.author_of(terms, P.w2pi)
            if author not in w2pi:
                w2pi[author] = 0
            if author == atw2:
                w2pi[author] += 1

            atwn,scorewn = P.author_of(terms, P.wpi_nomean)
            if author not in wpi_nomean:
                wpi_nomean[author] = 0
            if author == atwn:
                wpi_nomean[author] += 1

    #~ Results
    print 'n,', n
    print 'training_size,', training_size
    print 'testing_size,', testing_size
    print 'number_of_users,', number_of_users
    print 'L,',L
    print 'char_or_word,', char_or_word
    print 'case,',case
    print 'repetitions,', repetitions
    print 'author, spi, wpi, w2pi, wpi_nomean'
    print ''
    for author in users:
        print author,',',spi[author]/float(repetitions),',',wpi[author]/float(repetitions),',',w2pi[author]/float(repetitions),',',wpi_nomean[author]/float(repetitions)
        pass
    for p in P.profiles:
        print p, len(P.profiles[p])
if __name__ == '__main__':
    import sys
    n  = 1

    tr = 90
    ts = 10
    
    nu = 50

    pct = 10
    L = (tr+ts)*pct/100

    case = ''
    
    cw = 'word'
    rep= 10
    
    
    '''if len(sys.argv) >= 2:
        n = int(sys.argv[1])
    if len(sys.argv) >= 3:
        tr = int(sys.argv[2])
    if len(sys.argv) >= 4:
        ts = int(sys.argv[3])
    if len(sys.argv) >= 5:
        nu = int(sys.argv[4])
    if len(sys.argv) >= 6:
        cw = int(sys.argv[5])'''

    main(n, tr, ts, nu, L, cw, case, rep)

